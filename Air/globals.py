#!/usr/bin/env python

DEBUG = True


def log(data):
    global DEBUG
    if DEBUG == True:
        print(data)


def xor(data, startingPoint, bEndPoint):
    result = 0
    for i in range(startingPoint, bEndPoint):
        result ^= ord(data[i])
    return result


class droneData:
    angx = 0
    angy = 0
    heading = 0
    lat = 0
    lng = 0
    alt = 0
    serialErrorCount = 0


class wpCodes:  # Waypoint Codes

    AC_WAYPOINT = 1  # action codes: https://github.com/iNavFlight/inav/wiki/MSP-Navigation-Messages
    AC_POSHOLD_TIME = 3
    AC_RTH = 4

    FLAG_LAST = 0xa5
