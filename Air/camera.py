#!/usr/bin/env python

import pigpio

from globals import *


class Camera():
    cameraServo = pigpio.pi()
    neutralDC = 7.5  # neutral duty cycle
    cameraPosition = True

    def __init__(self):
        self.cameraServo.hardware_PWM(18, 50, int(self.neutralDC * 10000))  # 7.5%

    def stabilize(self):
        if self.cameraPosition == True:
            degrees = droneData.angy / 10
            self.cameraServo.hardware_PWM(18, 50, int(
                (self.neutralDC - degrees * 5 / 90) * 10000))  # duty cycle neutralDC + 5 ... 90 degrees
        else:
            self.cameraServo.hardware_PWM(18, 50, int((
                                                              self.neutralDC + 80 * 5 / 90) * 10000))  # duty cycle for -80 degrees, if I choose 90 degrees the servo stutters

    def togglePosition(self):
        if self.cameraPosition == True:
            self.cameraPosition = False
        else:
            self.cameraPosition = True
