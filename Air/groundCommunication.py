#!/usr/bin/env python

import struct
import json

from globals import *
from wfbConnection import UDPconnection

# .encode() with no args encodes with utf8 by default
# struct.unpack() returns a tuple
# if you don't understand fc.sendMessage look at fcConnection.py/fcConnection/sendMessage


class groundConnection:
    CONNECTION_CHECK = '0'
    DRONE_DATA = 'D'
    VIDEO = 'V'
    SET_RAW_RC = 'R'
    SET_WP = 'W'
    TOGGLE_CAMERA_POSITION = 'C'

    UDPc = UDPconnection()

    def checkConnection(self, fc):
        self.UDPc.sendReliable({'msgDirection': self.RPI2PC, 'msgType': self.CONNECTION_CHECK})

        cc = self.UDPc.receiveReliable()
        return
        # if cc is None:
        #     return
        # log("   I should've receive a 'Connection Check' response already, the response is: " + str(cc))
        # if cc.decode().rstrip('\r\n') != json.dumps({'msgDirection': self.PC2RPI, 'msgType': self.CONNECTION_CHECK}):
        #     print('CRITICAL: Connection Check FAILED! RTH initiated.')
        #     fc.setWP(wpCodes.AC_RTH)

    def sendDroneData(self):
        self.UDPc.sendUnreliable({'msgDirection': self.RPI2PC, 'msgType': self.DRONE_DATA,
                             'angx': droneData.angx, 'angy': droneData.angy, 'heading': droneData.heading,
                             'lat': droneData.lat, 'lng': droneData.lng, 'alt': droneData.alt,
                             'serialErrorCount': droneData.serialErrorCount})

    # to add sendVideo

    def getCommands(self, fc, camera):
        reliableData = self.UDPc.receiveReliable()
        unreliableData = self.UDPc.receiveUnreliable()

        if reliableData is not None:
            log('   received reliably data: ' + repr(reliableData))

            if reliableData['msgDirection'] == self.PC2RPI and reliableData['msgType'] == self.SET_WP:
                if reliableData['action'] == wpCodes.AC_WAYPOINT:
                    fc.setWP(reliableData['action'], number=reliableData['number'], lat=reliableData['lat'],
                             lng=reliableData['lng'], alt=reliableData['alt'],
                             p1=reliableData['p1'], p2=reliableData['p2'], p3=reliableData['p3'],
                             flag=reliableData['flag'])
                elif reliableData['action'] == wpCodes.AC_RTH:
                    fc.setWP(reliableData['action'])

            elif reliableData['msgDirection'] == self.PC2RPI and reliableData['msgType'] == self.TOGGLE_CAMERA_POSITION:
                camera.togglePosition()

        if unreliableData is not None:
            log('   received unreliably data: ' + repr(unreliableData))
            if unreliableData['M'] == self.SET_RAW_RC:
                fc.setRawRC(unreliableData['r'], unreliableData['p'], unreliableData['y'], unreliableData['t'])
