#!/usr/bin/env python
import asyncio

try:
    import time

    from globals import *
    from fcConnection import fcConnection
    from camera import Camera
    from groundCommunication import groundConnection

    fc = fcConnection()
    groundCon = groundConnection()
    camera = Camera()
    startTime = time.time()
    log(repr(startTime))
    time1 = startTime
    time2 = startTime


    async def main():
        await asyncio.gather(dataPolling(), sendDroneData(), getCommands())


    async def dataPolling():
        await asyncio.sleep(0.1)
        while True:
            fc.gatherData()
            camera.stabilize()
            await asyncio.sleep(0.1)


    async def sendDroneData():
        await asyncio.sleep(0.5)
        while True:
            groundCon.sendDroneData()
            await asyncio.sleep(0.5)


    async def getCommands():
        await asyncio.sleep(0.2)
        while True:
            groundCon.getCommands(fc, camera)
            await asyncio.sleep(0.2)


    # groundCon.checkConnection(fc)

    loop = asyncio.get_event_loop()
    asyncio.ensure_future(main())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        groundCon.UDPc.rs.close()
        groundCon.UDPc.us.close()

except (KeyboardInterrupt, SystemExit):
    pass
