from serial import Serial
import struct
import ctypes
import time

MSP_SET_RAW_RC = 200
MSP_RC = 105

header = b'$M<'
ser = Serial('COM3', baudrate=115200)


def xor(data, startingPoint, bEndPoint):
    result = 0
    for i in range(startingPoint, bEndPoint):
        result ^= ord(data[i])
    return result


def sendMessage(message_type, data, dnob):  # dnob - data: number of bytes
    print('   sending message to FC with message type: ' + str(message_type))
    # making the output
    dataLength = len(data)
    bytesLength = 0
    print('DataLength: ' + str(dataLength))
    output = ctypes.create_string_buffer(512)

    for i in range(0, dataLength):
        if dnob[i] == 1:
            print('bytes sent: 1')
            struct.pack_into('<B', output, 5 + bytesLength, data[i])
        elif dnob[i] == 2:
            # print('bytes sent: 2')
            struct.pack_into('<h', output, 5 + bytesLength, data[i])
        elif dnob[i] == 4:
            print('bytes sent: 4')
            struct.pack_into('<i', output, 5 + bytesLength, data[i])
        bytesLength += dnob[i]

    # Documentation: https://docs.python.org/3/library/struct.html
    struct.pack_into('<3sBB', output, 0, header, bytesLength, message_type)

    print('BytesLength: ' + str(bytesLength))
    # sending the data
    for i in range(0, 5 + bytesLength):
        ser.write(output[i])
        print(str(output[i]))
    print('check after last byte: ' + str(output[6 + bytesLength]))
    ser.write(struct.pack('<B', xor(output, 3, 5 + bytesLength)))


timeout = time.time() + 1
while time.time() < timeout:
    sendMessage(MSP_SET_RAW_RC,
                [1500, 1500, 1000, 1500, 1000],
                [2, 2, 2, 2, 2])
    time.sleep(0.05)

while True:
    sendMessage(MSP_SET_RAW_RC,
                [1500, 1500, 1000, 1500, 1800],
                [2, 2, 2, 2, 2])
    time.sleep(0.05)
    ser.timeout = 0.03
    ser.read(6)  # skipping the response check for a lower latency
    time.sleep(0.05)

# print(struct.unpack('<3sBBB', ser.read(6)))
# ser.timeout = None

# sendMessage(MSP_RC, [], [])
# ser.read(3)
# databytes = struct.unpack('<B', ser.read())[0]
# ser.read(1)
# d1 = struct.unpack('<h', ser.read(2))[0]
# d2 = struct.unpack('<h', ser.read(2))[0]
# d3 = struct.unpack('<h', ser.read(2))[0]
# d4 = struct.unpack('<h', ser.read(2))[0]
# d5 = struct.unpack('<h', ser.read(2))[0]
# d6 = struct.unpack('<h', ser.read(2))[0]
# d7 = struct.unpack('<h', ser.read(2))[0]
# d8 = struct.unpack('<h', ser.read(2))[0]
# ser.read(21)
# print(str(databytes) + ' ' + str(d1) + ' ' + str(d2) + ' ' + str(d3) + ' ' + str(d4) + ' ' + str(d5) + ' ' +
#       str(d6) + ' ' + str(d7) + ' ' + str(d8))
