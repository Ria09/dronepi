import time
import logging
import serial
import threading
import struct  # for decoding data strings

ser = serial.Serial()
ser.port = 'COM3'
ser.baudrate = 115200
ser.bytesize = serial.EIGHTBITS
ser.parity = serial.PARITY_NONE
ser.stopbits = serial.STOPBITS_ONE
ser.timeout = 0
ser.xonxoff = False
ser.rtscts = False
ser.dsrdtr = False
ser.writeTimeout = 2

timeMSP = 0.02

rcData = [1500, 1500, 1500, 1500]  # order -> roll, pitch, yaw, throttle

CMD2CODE = {
    # Getter
    'MSP_IDENT': 100,
    'MSP_STATUS': 101,
    'MSP_RAW_IMU': 102,
    'MSP_SERVO': 103,
    'MSP_MOTOR': 104,
    'MSP_RC': 105,
    'MSP_RAW_GPS': 106,
    'MSP_COMP_GPS': 107,
    'MSP_ATTITUDE': 108,
    'MSP_ALTITUDE': 109,
    'MSP_ANALOG': 110,
    'MSP_RC_TUNING': 111,
    'MSP_PID': 112,
    'MSP_BOX': 113,
    'MSP_MISC': 114,
    'MSP_MOTOR_PINS': 115,
    'MSP_BOXNAMES': 116,
    'MSP_PIDNAMES': 117,
    'MSP_WP': 118,
    'MSP_BOXIDS': 119,

    # Setter
    'MSP_SET_RAW_RC': 200,
    'MSP_SET_RAW_GPS': 201,
    'MSP_SET_PID': 202,
    'MSP_SET_BOX': 203,
    'MSP_SET_RC_TUNING': 204,
    'MSP_ACC_CALIBRATION': 205,
    'MSP_MAG_CALIBRATION': 206,
    'MSP_SET_MISC': 207,
    'MSP_RESET_CONF': 208,
    'MSP_SET_WP': 209,
    'MSP_SWITCH_RC_SERIAL': 210,
    'MSP_IS_SERIAL': 211,
    'MSP_DEBUG': 254,
}


def setRC():
    sendData(8, CMD2CODE["MSP_SET_RAW_RC"], rcData)
    time.sleep(timeMSP)


def sendData(data_length, code, data):
    checksum = 0
    total_data = ['$', 'M', '<', data_length, code] + data
    for i in struct.pack('<2B%dh' % len(data), *total_data[3:len(total_data)]):
        checksum = checksum ^ ord(i)

    total_data.append(checksum)
    b = None
    b = ser.write(struct.pack('<3c2B%dhB' % len(data), *total_data))
    return b


while True:
    setRC()
