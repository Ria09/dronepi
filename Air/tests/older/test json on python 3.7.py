import json

CONNECTION_CHECK = '0'
DRONE_DATA = 'D'
VIDEO = 'V'
SET_RAW_RC = 'R'
SET_WP = 'W'
TOGGLE_CAMERA_POSITION = 'C'

RPI2PC = '>'
PC2RPI = '<'
print(json.dumps({'msgDirection': RPI2PC, 'msgType': CONNECTION_CHECK}))
