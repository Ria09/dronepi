#!/usr/bin/env python

from serial import Serial
import time

from fcConnectionTest import fcConnection
from globalsTest import droneData

HC12ser = Serial('/dev/ttyAMA0', baudrate=115200)  # to consult .tests/HC-12test.py

HC12ser.write(b'sending test 1/r/n')
try:
    FCSer = fcConnection()
    FCSer.gatherData()
    HC12ser.write(('heading: ' + str(droneData.heading) + '/r/n').encode())

except Exception as e:
    HC12ser.write((str(e) + '/r/n').encode())
HC12ser.write(b'sending test 2/r/n')
