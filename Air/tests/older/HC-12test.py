import time
import serial

ser = serial.Serial(

    port='COM4',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)
ser.write(b'AT')
x = ser.read(2)
print(x)
