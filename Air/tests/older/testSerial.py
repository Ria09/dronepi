from serial import Serial
import struct

ser = Serial('/dev/ttyAMA0', baudrate=115200, timeout=5)

x = ser.read(3)
print(struct.unpack('<3s', x))
