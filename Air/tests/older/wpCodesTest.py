#!/usr/bin/env python


class wpCodes:  # Waypoint Codes

    AC_WAYPOINT = 1  # action codes: https://github.com/iNavFlight/inav/wiki/MSP-Navigation-Messages
    AC_POSHOLD_TIME = 3
    AC_RTH = 4

    FLAG_LAST = 0xa5
