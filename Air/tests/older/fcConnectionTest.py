#!/usr/bin/env python
# encoding=utf-8

from serial import Serial
import struct
import ctypes
import time

time.sleep(15)

from globalsTest import *
from wpCodesTest import wpCodes


# struct.unpack() returns a tuple


# FCC stands for flight controller communication
class fcConnection:
    header = b'$M<'
    ser = Serial('/dev/ttyACM0')

    MSP_IDENT = 100
    MSP_STATUS = 101
    MSP_RAW_IMU = 102
    MSP_SERVO = 103
    MSP_MOTOR = 104
    MSP_RC = 105
    MSP_RAW_GPS = 106
    MSP_COMP_GPS = 107
    MSP_ATTITUDE = 108
    MSP_ALTITUDE = 109
    MSP_ANALOG = 110
    MSP_RC_TUNING = 111
    MSP_PID = 112
    MSP_BOX = 113
    MSP_MISC = 114
    MSP_MOTOR_PINS = 115
    MSP_BOXNAMES = 116
    MSP_PIDNAMES = 117
    MSP_WP = 118
    MSP_BOXIDS = 119
    MSP_SERVO_CONF = 120

    MSP_SET_RAW_RC = 200
    MSP_SET_RAW_GPS = 201
    MSP_SET_PID = 202
    MSP_SET_BOX = 203
    MSP_SET_RC_TUNING = 204
    MSP_ACC_CALIBRATION = 205
    MSP_MAG_CALIBRATION = 206
    MSP_SET_MISC = 207
    MSP_RESET_CONF = 208
    MSP_SET_WP = 209
    MSP_SELECT_SETTING = 210
    MSP_SET_HEAD = 211  # Not used
    MSP_SET_SERVO_CONF = 212
    MSP_SET_MOTOR = 214
    MSP_BIND = 241

    MSP_EEPROM_WRITE = 250

    MSP_DEBUGMSG = 253
    MSP_DEBUG = 254

    def __init__(self):
        self.ser.timeout = None

    def sendMessage(self, message_type, data, dnob):  # dnob - data: number of bytes
        log('sending message to FC with message type: ' + str(message_type))
        # making the output
        length = len(data)
        output = ctypes.create_string_buffer(512)

        # Documentation: https://docs.python.org/3/library/struct.html
        struct.pack_into('<3sBB', output, 0, self.header, length, message_type)

        for i in range(0, length):
            if dnob[i] == 1:
                struct.pack_into('<B', output, 5 + i, data[i])
            elif dnob[i] == 2:
                struct.pack_into('<h', output, 5 + i, data[i])
            elif dnob[i] == 4:
                struct.pack_into('<i', output, 5 + i, data[i])

        # sending the data
        for i in range(0, 5 + length):
            self.ser.write(output[i])
        self.ser.write(struct.pack('<B', xor(output, 3, 5 + length)))

    def processMessage(self, t_message_type, t_length):

        message_type = struct.unpack('<B', t_message_type)
        log("in the message processing stage, I've found out that the received message type is " + str(message_type[0]))
        if message_type[0] == self.MSP_ATTITUDE:
            t_angx = self.ser.read(2)
            t_angy = self.ser.read(2)
            t_heading = self.ser.read(2)

            t_receivedXOR = self.ser.read(1)
            receivedXOR = struct.unpack('<B', t_receivedXOR)[0]
            if receivedXOR == xor((
                    t_message_type,
                    t_length,
                    t_angx[0:1], t_angx[1:2],  # https://pythonhosted.org/bitstring/slicing.html
                    t_angy[0:1], t_angy[1:2],
                    t_heading[0:1], t_heading[1:2]
            ), 0, 8):
                droneData.angx = struct.unpack('<h', t_angx)[0]  # gives data in tuple
                droneData.angy = struct.unpack('<h', t_angy)[0]
                droneData.heading = struct.unpack('<h', t_heading)[0]
                print(('heading: ' + str(droneData.heading) + '/r/n').encode())

            else:
                self.serialError()

        elif message_type[0] == self.MSP_RAW_GPS:  # returns only lat, lng and att
            t_gps_fix = self.ser.read(1)
            t_num_sat = self.ser.read(1)
            t_lat = self.ser.read(4)
            t_lng = self.ser.read(4)
            t_alt = self.ser.read(2)
            t_speed = self.ser.read(2)
            t_ground_course = self.ser.read(2)

            t_receivedXOR = self.ser.read(1)
            receivedXOR = struct.unpack('<B', t_receivedXOR)[0]
            if receivedXOR == xor((
                    t_message_type,
                    t_length,
                    t_gps_fix,
                    t_num_sat,
                    t_lat[0:1], t_lat[1:2], t_lat[2:3], t_lat[3:4],  # https://pythonhosted.org/bitstring/slicing.html
                    t_lng[0:1], t_lng[1:2], t_lng[2:3], t_lng[3:4],
                    t_alt[0:1], t_alt[1:2],
                    t_speed[0:1], t_speed[1:2],
                    t_ground_course[0:1], t_ground_course[1:2]
            ), 0, 18):

                droneData.lat = struct.unpack('<i', t_lat)[0]
                droneData.lng = struct.unpack('<i', t_lng)[0]
                droneData.alt = struct.unpack('<i', t_alt)[0]

            else:
                self.serialError()

    def receiveMessage(self):
        receivedHeader = self.ser.read(3)
        # unimportant: log('received header: ' + str(receivedHeader))
        if receivedHeader == b'$M>':  # if receivedHeader really is a FC>RPI header:
            log('received a message from the RPI that has the correct header')
            t_length = self.ser.read(1)
            t_message_type = self.ser.read(1)
            self.processMessage(t_message_type, t_length)
        else:
            self.serialError()

    def gatherData(self):

        self.sendMessage(self.MSP_ATTITUDE, [], [])  # request for attitude
        self.receiveMessage()

        # self.sendMessage(self.MSP_RAW_GPS, [], [])  # request for GPS data
        # self.receiveMessage()

    def wp(self, action, number=0, lat=0, lng=0, alt=0, p1=0, p2=0, p3=0, flag=0):
        if action == wpCodes.AC_RTH:
            self.sendMessage(self.MSP_SET_WP, [1, wpCodes.AC_RTH, 0, 0, 25, 0, 0, 0, wpCodes.FLAG_LAST],
                             [1, 1, 4, 4, 4, 2, 2, 2, 1])
        elif action == wpCodes.AC_WAYPOINT:
            self.sendMessage(self.MSP_SET_WP, [number, wpCodes.AC_WAYPOINT, lat, lng, alt, p1, p2, p3, flag],
                             [1, 1, 4, 4, 4, 2, 2, 2, 1])

    def serialError(self):
        droneData.serialErrorCount += 1
        print("fc serial communication errors:" + str(droneData))  # these errors will always be displayed
        # because they're critical
