import socket
import asyncio
import time
import json
import select
from operator import itemgetter
import random
import sys
from collections import deque

from globals import *


class UDPconnection:
    RADDR = ("10.5.0.1", 24)
    UADDR = ("127.0.0.1", 14560)

    rs = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    rs.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 16384)
    rs.connect(RADDR)

    us = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    us.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 16384)

    def sendReliable(self, data):
        self.rs.sendall(json.dumps(data).encode())
        log("sent data reliably")

    def receiveReliable(self):
        readable, writeable, exceptional = select.select([self.rs], [], [], 0)
        if not readable:
            return
        data = readable[0].recv(1024)
        data = json.loads(data.decode())
        return data

    def sendUnreliable(self, data):
        self.us.sendto(json.dumps(data).encode(), self.UADDR)
        log("sent data unreliably")

    def receiveUnreliable(self):
        readable, writeable, exceptional = select.select([self.us], [], [], 0)
        if not readable:
            return
        data, addr = readable[0].recvfrom(1024)
        log(repr(data))
        data = json.loads(data.decode())
        return data
