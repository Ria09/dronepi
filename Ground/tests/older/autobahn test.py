#!/usr/bin/python

from autobahn.asyncio.websocket import WebSocketClientProtocol, WebSocketClientFactory
import json
import random


class SlowSquareClientProtocol(WebSocketClientProtocol):

    def onMessage(self, payload, isBinary):
        if not isBinary:
            res = json.loads(payload.decode('utf8'))
            print("Result received: {}".format(res))
            self.sendMessage(json.dumps(random.randint(1, 2003)).encode('utf8'))

    def onClose(self, wasClean, code, reason):
        if reason:
            print(reason)
        loop.stop()


if __name__ == '__main__':
    import asyncio

    factory = WebSocketClientFactory(u"ws://127.0.0.1:8000")
    factory.protocol = SlowSquareClientProtocol

    loop = asyncio.get_event_loop()
    coro = loop.create_connection(factory, '127.0.0.1', 8000)
    loop.run_until_complete(coro)
    loop.run_forever()
loop.close()
