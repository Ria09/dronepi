#!/usr/bin/python

DEBUG = True


def log(data):
    global DEBUG
    if DEBUG == True:
        print(data)


def xor(data, startingPoint, bEndPoint):
    result = 0
    for i in range(startingPoint, bEndPoint):
        result ^= ord(data[i])
    return result


class droneData:
    angx = 0
    angy = 0
    heading = 0
    lat = 0
    lng = 0
    alt = 0
    serialErrorCount = 0


class toSend:
    RawRC = None
    WP = None

# class data:
#
#    receivedFromAir = False
#    receivedDataFromAir = None
#
#    receivedFromWebsite = False
#    receivedDataFromWebsite = None
#
#    toSendToAir = False
#    toSendToAirData = None
#
#    toSendToWebsite = False
#    toSendToWebsiteData = None
