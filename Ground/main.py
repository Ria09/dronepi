#!/usr/bin/python

try:
    from autobahn.asyncio.websocket import WebSocketServerProtocol
    from autobahn.asyncio.websocket import WebSocketServerFactory
    import asyncio
    import json
    import webbrowser
    import os
    import struct

    from globals import *
    from wfbConnection import UDPconnection

    UDPc = UDPconnection()

    # .encode() with no args encodes with utf8 by default

    HEARTBEAT = 'H'
    DRONE_DATA = 'D'
    VIDEO = 'V'
    SET_RAW_RC = 'R'
    SET_WP = 'W'
    TOGGLE_CAMERA_POSITION = 'C'


    class WebSocketServer(WebSocketServerProtocol):

        async def onConnect(self, request):
            print("Client connecting: {}".format(request.peer))

        async def onOpen(self):
            print("WebSocket connection open.")
            await asyncio.sleep(1)
            while True:
                # reading drone data
                # reliableData = UDPc.receiveReliable()
                unreliableData = UDPc.receiveUnreliable()
                if unreliableData is not None and unreliableData['msgType'] == DRONE_DATA:
                    log("   received drone data: " + repr(unreliableData))
                    self.sendMessage(json.dumps(
                        unreliableData).encode())
                    # https://autobahn.readthedocs.io/en/latest/websocket/programming.html#sending-messages
                await asyncio.sleep(0.2)  # 0.0322580645)

        async def onMessage(self, payload, isBinary):
            data = json.loads(payload.decode())
            # log('   received data from the website: ' + str(data))
            if data['M'] == SET_WP:
                UDPc.sendReliable(data)
            elif data['M'] == SET_RAW_RC:
                UDPc.sendUnreliable(data)

        async def onClose(self, wasClean, code, reason):
            print("WebSocket connection closed: {}".format(reason))


    factory = WebSocketServerFactory()
    factory.protocol = WebSocketServer

    loop = asyncio.get_event_loop()
    coroutine = loop.create_server(factory, '127.0.0.1', 8000)
    asyncio.ensure_future(coroutine)
    webbrowser.open('file://' + os.path.realpath('interface/index.html'))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        UDPc.rs.close()
        UDPc.us.close()

except (KeyboardInterrupt, SystemExit):
    pass
