#!/bin/bash
set +x



CORES=`grep -c ^processor /proc/cpuinfo`
J_ARG=`expr $CORES \* 3`
J_ARG=`expr $J_ARG / 2`
echo "value $J_ARG was chosen for -j"


echo "Specify device. (rpi0/rpi3 | Default: rpi0)"
read DEVICE
if [ "$DEVICE" == "" ]
then
	DEVICE="rpi0"
elif [ "$DEVICE" != "rpi0" ] && [ "$DEVICE" != "rpi3" ]
then
	echo "ERROR: Bad device value."
	read -p "Press Enter to exit... "
	exit 1
fi
echo "$DEVICE was chosen."



if [ "$DEVICE" == "rpi0" ]
then
	KERNEL="kernel"
else
	KERNEL="kernel7"
fi



echo "Specify kernel source folder. (Default: $KERNEL-linux-rpi-4.9.y-stable-$DEVICE)"
read KSLOCATION
if [ "$KSLOCATION" == "" ]
then
	KSLOCATION="$KERNEL-linux-rpi-4.9.y-stable-$DEVICE"
fi


if [ -d "$KSLOCATION" ]
then
	echo "$KSLOCATION was chosen"
else
	echo "ERROR: Folder does not exist."
	read -p "Press Enter to exit... "
	exit 1
fi
cd $KSLOCATION



echo "Specify kernel patches folder. (Default: kernel_patches_2)"
read KPLOCATION
if [ "$KPLOCATION" == "" ]
then
	KPLOCATION="kernel_patches_2"
fi


if [ -d "../$KPLOCATION" ]
then
	echo "$KPLOCATION was chosen"
else
	echo "ERROR: Folder does not exist."
	read -p "Press Enter to exit... "
	exit 1
fi


cat ../$KPLOCATION/* | patch -p1



if [ "$KERNEL" == "kernel" ]
then
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bcmrpi_defconfig -j $J_ARG
elif [ "$KERNEL"=="kernel7" ]
then
	make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bcm2709_defconfig -j $J_ARG
fi



make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules dtbs -j $J_ARG



echo "MAKE SURE THAT THE SD CARD'S PARTITIONS ARE MOUNTED INTO \/mnt/fat32 AND \/mnt/ext4!"
read -p "Press Enter to continue... "



sudo make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=\/mnt/ext4 modules_install -j $J_ARG



cd \/


sudo cp mnt/fat32/$KERNEL.img mnt/fat32/$KERNEL-backup.img
sudo cp /root/Desktop/rpi_kernel_setup/$KSLOCATION/arch/arm/boot/zImage \/mnt/fat32/$KERNEL.img
sudo cp /root/Desktop/rpi_kernel_setup/$KSLOCATION/arch/arm/boot/dts/*.dtb \/mnt/fat32/
sudo cp /root/Desktop/rpi_kernel_setup/$KSLOCATION/arch/arm/boot/dts/overlays/*.dtb* \/mnt/fat32/overlays/
sudo cp /root/Desktop/rpi_kernel_setup/$KSLOCATION/arch/arm/boot/dts/overlays/README \/mnt/fat32/overlays/



read -p "Do you want to unmount the partitions? (y/n | Default: n)" UM
if [ "$UM" == "y" ]
then
	sudo umount mnt/fat32
	sudo umount mnt/ext4
	echo "You may now disconnect the SD card."
fi
read -p "Press Enter to exit... "
